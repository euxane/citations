# Nix package for the "Papers Please" citation generator
#
# environment.systemPackages =
#   let citationGit = builtins.fetchGit {
#     url = "https://gitlab.com/Saphire/citations.git";
#     rev = "<some rev>";
#   };
#   in [ (pkgs.python3Packages.callPackage "${citationGit}/package/nix" {}) ];

{ buildPythonPackage, pillow, setuptools }:

buildPythonPackage rec {
  name = "citations";
  src = ../..;
  propagatedBuildInputs = [ pillow setuptools ];
}

